package main

import (
	"fmt"
	"math"
	"time"

	"github.com/faiface/pixel"
	"github.com/faiface/pixel/imdraw"
	"github.com/faiface/pixel/pixelgl"
	"github.com/faiface/pixel/text"
	"golang.org/x/image/colornames"
)

var Game GameStruct

func Debug(win *pixelgl.Window, code int) {
	imd := imdraw.New(nil)

	if code >= 7 {
		imd.SetColorMask(colornames.Yellow)
		imd.Push(Game.Camera.Unproject(Game.Window.MousePosition()))
		imd.Push(Game.Player.Box.Center())
		imd.Line(5)
	}

	if code >= 3 {
		imd.SetColorMask(colornames.Darkolivegreen)
		imd.Push(BoxOnGrid(Game.Player.Box).Min)
		imd.Push(BoxOnGrid(Game.Player.Box).Max)
		imd.Rectangle(5)
	}

	if code >= 2 {
		imd.SetColorMask(colornames.Navy)
		imd.Push(Game.Player.Box.Min)
		imd.Push(Game.Player.Box.Max)
		imd.Rectangle(3)
	}

	sx, sy := 0.0, 0.0

	if code >= 4 {
		imd.SetColorMask(colornames.Orangered)
		sx = math.Floor((Game.Window.Bounds().W()/Game.Zoom()/GridSize + 10)) * 2
		sy = math.Floor((Game.Window.Bounds().H()/Game.Zoom()/GridSize + 10)) * 2
		// for i := -sx / 2; i < sx/2; i++ {
		// 	for j := -sy / 2; j < sy/2; j++ {
		// 		imd.Push(Game.Player.BoxOnGrid().Center().Add(pixel.V(GridSize*i, GridSize*j)))
		// 		imd.Push(Game.Player.BoxOnGrid().Center().Add(pixel.V(GridSize*(i+1), GridSize*(j+1))))
		// 		imd.Rectangle(2)
		// 	}
		// }
		for i := -sx / 2; i <= sx/2; i++ {
			p := BoxOnGrid(Game.Player.Box).Min.Add(pixel.V(i, -sy/2).Scaled(GridSize))
			imd.Push(p)
			imd.Push(p.Add(pixel.V(0, sy*GridSize)))
			imd.Line(2)
			p.Add(pixel.V(GridSize, 0))
		}

		for i := -sy / 2; i <= sy/2; i++ {
			p := BoxOnGrid(Game.Player.Box).Min.Add(pixel.V(-sx/2, i).Scaled(GridSize))
			imd.Push(p)
			imd.Push(p.Add(pixel.V(sx*GridSize, 0)))
			imd.Line(2)
			p.Add(pixel.V(0, GridSize))
		}
	}

	if code >= 5 {
		imd.SetColorMask(colornames.Paleturquoise)
		imd.Push(pixel.V(0, 0))
		imd.Circle(10, 0)
	}

	if code >= 6 {
		imd.SetColorMask(colornames.Pink)
		k := 6.0
		sx = Game.Window.Bounds().W() / k
		sy = Game.Window.Bounds().H() / k
		for i := 0.0; i < k; i++ {
			for j := 0.0; j < k; j++ {
				imd.Push(Game.Camera.Unproject(pixel.V(sx*i, sy*j)))
				imd.Push(Game.Camera.Unproject(pixel.V(sx*(i+1), sy*(j+1))))
				imd.Rectangle(2)
			}
		}
	}

	imd.Draw(win)

	win.SetMatrix(pixel.IM)
	if code >= 1 {
		txt := text.New(pixel.V(0, 0), TextAtlas)
		txt.Color = colornames.Gray
		fmt.Fprintln(txt, Fps, "FPS")
		txt.Draw(win, pixel.IM.Moved(txt.Bounds().Center().Scaled(0.2)))
	}
	win.SetMatrix(Game.Camera)
}

var Fps float64 = -1

func UpdateDrawFps() {
	for true {
		//Fps = FloorE(1/Game.DT, -1)
		Fps = math.Max(0, FloorE(1/(Game.Clock.FpsSum/float64(Game.Clock.FpsCount)), -1))
		Game.Clock.FpsSum = 0
		Game.Clock.FpsCount = 0
		fmt.Println("FPS:", Fps)
		time.Sleep(time.Second / 5)
	}
}

func Control(win *pixelgl.Window, dt float64) {
	mousePos := Game.Camera.Unproject(Game.Window.MousePosition())

	dx, dy := 0.0, 0.0
	if Game.Window.Pressed(pixelgl.KeyW) {
		dy++
	}
	if Game.Window.Pressed(pixelgl.KeyS) {
		dy--
	}
	if Game.Window.Pressed(pixelgl.KeyD) {
		dx++
	}
	if Game.Window.Pressed(pixelgl.KeyA) {
		dx--
	}
	if Game.Window.JustPressed(pixelgl.KeySpace) {
		Game.Player.Sprite.Set(Game.Pictures["player_action"], Game.Player.Sprite.Frame())
	}
	if Game.Window.JustReleased(pixelgl.KeySpace) {
		Game.Player.Sprite.Set(Game.Pictures["player"], Game.Player.Sprite.Frame())
	}
	if Game.Window.Pressed(pixelgl.KeySpace) {

	}
	if Game.Window.JustReleased(pixelgl.KeyR) {
		Game.LoadLevel(Game.Levels[Game.CurrentLevel])
	}
	Game.Player.Control(pixel.V(dx, dy), mousePos, false, false, dt)

	// defaultZoom := 1.0
	// unzoomedZoom := 0.6
	// lerpSpeed := 0.4
	// if Game.Window.Pressed(pixelgl.MouseButtonRight) {
	// 	Game.Zoom = RoundTo(Lerp(Game.Zoom, unzoomedZoom, lerpSpeed), unzoomedZoom, 1e-3)
	// } else {
	// 	Game.Zoom = RoundTo(Lerp(Game.Zoom, defaultZoom, lerpSpeed), defaultZoom, 1e-3)
	// }
}

func NextLevel(g *GameStruct, won bool) {
	if won {
		Game.Window.SetMatrix(pixel.IM)
		Game.Window.Clear(colornames.Black)
		DrawText("AW YEAS!",
			Game.Window.Bounds().Center(),
			pixel.V(1, 1).Scaled(5),
			colornames.Whitesmoke,
			Game.Window,
		)
		Game.Window.Update()
		time.Sleep(time.Second * 3)
		Game.Window.SetMatrix(Game.Camera)
	}
	g.CurrentLevel++
	if g.CurrentLevel >= len(g.Levels) {
		Game.Window.SetMatrix(pixel.IM)
		Game.Window.Clear(colornames.Black)
		DrawText("YOU WIN!!!",
			Game.Window.Bounds().Center(),
			pixel.V(1, 1).Scaled(5),
			colornames.Whitesmoke,
			Game.Window,
		)
		time.Sleep(time.Second * 5)
		DrawText(".\n\nthe end.",
			Game.Window.Bounds().Center(),
			pixel.V(1, 1).Scaled(5),
			colornames.White,
			Game.Window,
		)
		time.Sleep(time.Second * 1)
		Game.Window.SetMatrix(Game.Camera)
		Game.Window.SetClosed(true)
	} else {
		g.LoadLevel(g.Levels[g.CurrentLevel])
	}
}

func Run() {
	var err error
	Game.Window, err = pixelgl.NewWindow(pixelgl.WindowConfig{
		Bounds: pixel.R(0, 0, Resolution.X, Resolution.Y),
		Title:  "Ug!",
	})
	if err != nil {
		panic(err)
	}

	Game.Init()
	NextLevel(&Game, false)

	Framerate := time.Tick(time.Second / 240)
	LastUpdate := time.Now()
	go UpdateDrawFps()
	go Game.MoveFolks()
	for !Game.Window.Closed() {
		Game.Window.Clear(colornames.White)

		dt := time.Since(LastUpdate).Seconds()
		LastUpdate = time.Now()
		Game.Update(dt)

		Game.Draw(Game.Window)

		if Game.Folks.Left == 0 {
			NextLevel(&Game, true)
			LastUpdate = time.Now()
		}

		Game.Window.Update()
		<-Framerate
	}
}

func main() {
	pixelgl.Run(Run)
}
