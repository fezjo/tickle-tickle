package main

import (
	"fmt"
	"image"
	"io/ioutil"
	"os"

	_ "image/png"

	"github.com/faiface/pixel"
	"github.com/golang/freetype/truetype"
	"golang.org/x/image/font"
)

type PictureMap map[string]pixel.Picture

func (pm PictureMap) Load(imgPath, imgName string) (pixel.Picture, error) {
	pic, ok := pm[imgName]
	if ok {
		fmt.Printf("WARNING: Picture already loaded - '%v' [%v]\n", imgName, imgPath)
		return pm[imgName], nil
	}

	pic, err := LoadPicture(imgPath)
	if err != nil {
		fmt.Printf("ERROR: Couldn't load image - '%v' [%v] -> \n\t -> %v\n", imgName, imgPath, err)
		pm[imgName] = pixel.MakePictureData(pixel.R(0, 0, 1, 1))
		return pm[imgName], err
	}

	pm[imgName] = pic
	return pm[imgName], nil
}

func LoadPicture(imgPath string) (pixel.Picture, error) {
	file, err := os.Open(imgPath)
	defer file.Close()
	if err != nil {
		return nil, err
	}

	img, _, err := image.Decode(file)
	if err != nil {
		return nil, err
	}

	return pixel.PictureDataFromImage(img), nil
}

func LoadTTF(path string, size float64) (font.Face, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	bytes, err := ioutil.ReadAll(file)
	if err != nil {
		return nil, err
	}

	font, err := truetype.Parse(bytes)
	if err != nil {
		return nil, err
	}

	return truetype.NewFace(font, &truetype.Options{
		Size:              size,
		GlyphCacheEntries: 1,
	}), nil
}
