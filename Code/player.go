package main

import (
	"math"

	"github.com/faiface/pixel"
)

var (
	PlayerVel  = 2.0
	PlayerSize = pixel.V(1, 1).Scaled(GridSize)
)

type PlayerStruct struct {
	Box          pixel.Rect
	Vel          pixel.Vec
	Rot          float64
	DrawPosition pixel.Vec
	Sprite       *pixel.Sprite
}

func NewPlayer(pos, siz pixel.Vec, imgName string) *PlayerStruct {
	p := PlayerStruct{}

	p.Box = pixel.R(pos.X, pos.Y, pos.X+siz.X, pos.Y+siz.Y)
	p.DrawPosition = p.Box.Center()

	p.Rot = math.Pi / 2

	p.Sprite = pixel.NewSprite(Game.Pictures[imgName], Game.Pictures[imgName].Bounds())

	return &p
}

func (p *PlayerStruct) Control(move, aim pixel.Vec, jumped, shoot bool, dt float64) {
	p.Vel = pixel.ZV
	if move.Len() != 0 {
		p.Vel = move.Unit().Scaled(GridSize).Scaled(PlayerVel)
		p.Rot = p.Vel.Angle()
	}
}

func (p *PlayerStruct) Update(dt float64) {
	for _, s := range []pixel.Vec{pixel.V(0, 1), pixel.V(1, 0)} {
		v := p.Vel.ScaledXY(s)
		if v.Len() > 0 {
			moved := p.Box.Moved(v.Scaled(dt))
			if !Collided(moved) {
				p.Box = moved
			}
		} else {
			p.Box = p.Box.Moved(PosOnGrid(p.Box.Min).Sub(p.Box.Min).ScaledXY(s))
		}
	}
	p.DrawPosition = LerpVec(p.DrawPosition, BoxOnGrid(p.Box).Center(), math.Pow(0.985, 1/dt))
}

func (p *PlayerStruct) Draw(target pixel.Target) {
	DrawObjekt(
		p.DrawPosition,
		p.Box.Size(),
		p.Rot,
		p.Sprite,
		target,
	)
}

//!!

func Collided(box pixel.Rect) bool {
	x, y := IndexOnGrid(box)

	return Game.Grid[y][x].occupied
}

func (p *PlayerStruct) Catch(folks FolksMap) {
	for _, f := range folks {
		if p.Box.Intersect(f.Box).Area() > 1 {
			f.Held = true
		} else {
			f.Held = false
		}
	}
}
