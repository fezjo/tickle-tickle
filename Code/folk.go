package main

import (
	"math"

	"golang.org/x/image/colornames"

	"github.com/faiface/pixel"
	"github.com/faiface/pixel/imdraw"
)

var (
	MaxClothProgress = 9
)

type FolkStruct struct {
	Box             pixel.Rect
	Vel             pixel.Vec
	Rot             float64
	DesiredPostion  pixel.Vec
	DesiredRotation float64
	Sprite          *pixel.Sprite
	Typ             string
	Held            bool
	ClothProgress   int
	Caught          bool
	Id              int64
}

func NewFolkStruct(pos, siz pixel.Vec, imgName string, typ string) *FolkStruct {
	f := FolkStruct{}

	f.Id = ObjektID
	ObjektID++

	f.Box = pixel.R(pos.X, pos.Y, pos.X+siz.X, pos.Y+siz.Y)
	f.DesiredPostion = f.Box.Center()
	f.Sprite = pixel.NewSprite(Game.Pictures[imgName], Game.Pictures[imgName].Bounds())
	f.Typ = typ

	return &f
}

func (f *FolkStruct) Draw(target pixel.Target) {
	DrawObjekt(
		f.Box.Center(),
		f.Box.Size(),
		f.Rot,
		f.Sprite,
		target,
	)
}

func (f *FolkStruct) Update(dt float64) {
	f.Rot = RoundTo(Lerp(f.Rot, f.DesiredRotation, math.Pow(0.99, 1/dt)), f.DesiredRotation, 1e-3)

	f.Box = BoxMovedTo(f.Box, LerpVec(f.Box.Center(), f.DesiredPostion, math.Pow(0.99, 1/dt)))
}

func (f *FolkStruct) AI(game *GameStruct) bool {
	if f.Caught {
		return false
	}

	if f.Held {
		f.ClothProgress++
		if f.ClothProgress >= MaxClothProgress {
			f.Caught = true
			f.Sprite.Set(Game.Pictures["folk_morfed"], Game.Pictures["folk_morfed"].Bounds())
			return true
		}
	} else {
		f.ClothProgress = 0
	}

	for i := 0; i < 1; i++ {
		moved := BoxOnGrid(f.Box.Moved(pixel.V(GridSize, 0).Rotated(f.Rot)))
		x, y := IndexOnGrid(moved)

		if !game.Grid[y][x].occupied {
			f.Held = false
			f.ClothProgress = 0
			f.DesiredPostion = moved.Center()
			return false
		} else if i == 0 {
			if f.Typ == "<" {
				f.DesiredRotation += math.Pi / 2
			} else if f.Typ == ">" {
				f.DesiredRotation -= math.Pi / 2
			} else if f.Typ == "v" {
				f.DesiredRotation += math.Pi
			}
		}
	}
	return false
}

func UpdateFolks(folks FolksMap, dt float64) {
	for _, f := range folks {
		f.Update(dt)
	}
}

type FolksMap map[int64]*FolkStruct

type FolksStruct struct {
	F    FolksMap
	Imd  *imdraw.IMDraw
	Left int
}

func NewFolksStruct(imgName string) FolksStruct {
	fs := FolksStruct{}
	fs.F = make(FolksMap)
	fs.Imd = imdraw.New(Game.Pictures[imgName])

	return fs
}

func (fs *FolksStruct) Draw(target pixel.Target) {
	fs.Imd.Clear()
	for _, f := range fs.F {
		fs.Imd.SetColorMask(colornames.Greenyellow)
		fs.Imd.Push(f.Box.Min)
		fs.Imd.Push(f.Box.Max.Sub(pixel.V(
			0,
			f.Box.H()*(1-float64(f.ClothProgress)/float64(MaxClothProgress)),
		)))
		fs.Imd.Rectangle(0)
		fs.Imd.SetColorMask(colornames.White)
	}
	fs.Imd.Draw(target)

	for _, f := range fs.F {
		f.Draw(target)
	}
}
