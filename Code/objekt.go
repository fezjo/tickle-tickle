package main

import (
	"github.com/faiface/pixel"
	"github.com/faiface/pixel/imdraw"
)

var ObjektID int64 = 0

type ObjektStruct struct {
	Box    pixel.Rect
	Rot    float64
	Sprite *pixel.Sprite
	Id     int64
}

func NewObjektStruct(pos, siz pixel.Vec, imgName string) *ObjektStruct {
	obj := ObjektStruct{}

	obj.Id = ObjektID
	ObjektID++

	obj.Box = pixel.R(pos.X, pos.Y, pos.X+siz.X, pos.Y+siz.Y)
	obj.Sprite = pixel.NewSprite(Game.Pictures[imgName], Game.Pictures[imgName].Bounds())

	return &obj
}

func (o *ObjektStruct) Draw(target pixel.Target) {
	DrawObjekt(
		o.Box.Center(),
		o.Box.Size(),
		o.Rot,
		o.Sprite,
		target,
	)
}

type ObjektsMap map[int64]*ObjektStruct

type ObjektsStruct struct {
	O   ObjektsMap
	Imd *imdraw.IMDraw
}

func NewObjektsStruct(imgName string) ObjektsStruct {
	o := ObjektsStruct{}
	o.O = make(ObjektsMap)
	o.Imd = imdraw.New(Game.Pictures[imgName])

	return o
}

func (o *ObjektsStruct) Draw(target pixel.Target) {
	o.Imd.Clear()
	for _, obj := range o.O {
		obj.Draw(o.Imd)
	}
	o.Imd.Draw(target)
}
