package main

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"time"

	"github.com/faiface/pixel/imdraw"

	"github.com/faiface/pixel"
	"github.com/faiface/pixel/pixelgl"
	"github.com/faiface/pixel/text"
)

var (
	GridSize           = 40.0
	GameStepsPerSecond = 3.0
)

type GameStruct struct {
	Window   *pixelgl.Window
	Camera   pixel.Matrix
	Pictures PictureMap

	Grid       [][]TileStruct
	GridSizePx pixel.Vec

	Player PlayerStruct
	Walls  ObjektsStruct
	Folks  FolksStruct

	FloorImd    *imdraw.IMDraw
	FloorSprite *pixel.Sprite

	Clock ClockStruct

	Levels       []string
	CurrentLevel int
}

var TextAtlas *text.Atlas

func (g *GameStruct) Init() {
	pref := "./Levels/"

	file, err := os.Open(pref + "LEVELS")
	if err != nil {
		panic(err)
	}
	defer file.Close()
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		g.Levels = append(g.Levels, pref+scanner.Text())
	}
	fmt.Println(g.Levels)
	g.CurrentLevel = -1

	pref = "./Resources/"
	g.Pictures = make(map[string]pixel.Picture)
	g.Pictures.Load(pref+"player.png", "player")
	g.Pictures.Load(pref+"player_action.png", "player_action")
	g.Pictures.Load(pref+"wall.png", "wall")
	g.Pictures.Load(pref+"folk.png", "folk")
	g.Pictures.Load(pref+"folk_morfed.png", "folk_morfed")
	g.Pictures.Load(pref+"floor.png", "floor")

	g.FloorImd = imdraw.New(g.Pictures["floor"])
	g.FloorSprite = pixel.NewSprite(g.Pictures["floor"], g.Pictures["floor"].Bounds())

	face, err := LoadTTF(pref+"arial.ttf", 20)
	if err != nil {
		fmt.Println(err)
	}
	TextAtlas = text.NewAtlas(face, text.ASCII)

	g.Walls = NewObjektsStruct("wall")
	g.Folks = NewFolksStruct("folk")
}

func (g *GameStruct) LoadLevel(lvlPath string) {
	f, err := os.Open(lvlPath)
	defer f.Close()
	if err != nil {
		fmt.Printf("ERROR: Couldn't load level - '%v' -> \n\t -> %v\n", lvlPath, err)
		return
	}

	var w, h int
	fmt.Fscan(f, &w, &h)
	Game.GridSizePx = pixel.V(float64(w), float64(h)).Scaled(GridSize)

	Game.Grid = make([][]TileStruct, h)
	for i := 0; i < h; i++ {
		Game.Grid[i] = make([]TileStruct, w)
	}

	Game.Walls.O = make(ObjektsMap)
	Game.Folks.F = make(FolksMap)
	Game.Folks.Left = 0

	folks := make(map[rune]*FolkStruct)

	c := '$'
	for y := h - 1; y >= 0; y-- {
		for x := 0; x < w; x++ {
			fmt.Fscanf(f, "%c", &c)
			pos := pixel.V(float64(x), float64(y)).Scaled(GridSize)
			siz := pixel.V(1, 1).Scaled(GridSize)
			switch {
			case c == '.':
				Game.Grid[y][x] = TileStruct{"free", false}
			case c == '#':
				g.Walls.O[ObjektID-1] = NewObjektStruct(pos, siz, "wall")
				Game.Grid[y][x] = TileStruct{"wall", true}
			case c == 'P':
				Game.Player = *NewPlayer(
					pos,
					PlayerSize,
					"player",
				)
				Game.Grid[y][x] = TileStruct{"player", true}
			case c == 'F':
				// firepit
			case 'a' <= c && c <= 'z':
				folks[c] = NewFolkStruct(pos, siz, "folk", string(c))
				Game.Folks.F[folks[c].Id] = folks[c]
				Game.Grid[y][x] = TileStruct{"folk", true}
			default:
				fmt.Printf("WARNING: Unrecognized level object - '%c' [%v]\n", c, c)
			}
		}
		fmt.Fscanf(f, "\n")
	}

	dir, typ := '$', '$'
	for i := 0; i < len(folks); i++ {
		fmt.Fscanf(f, "%c %c %c\n", &c, &dir, &typ)
		folks[c].Typ = string(typ)
		folks[c].Rot = float64(
			0*BoolToInt(dir == '>')+
				1*BoolToInt(dir == '^')+
				2*BoolToInt(dir == '<')+
				3*BoolToInt(dir == 'v')) * math.Pi / 2
		folks[c].DesiredRotation = folks[c].Rot
		g.Folks.Left++
		fmt.Printf("%c %c %c %f\n", c, dir, typ, folks[c].Rot)
	}
}

func (g *GameStruct) Update(dt float64) {
	g.Clock.Dt = dt
	g.Clock.FpsSum += dt
	g.Clock.FpsCount++

	g.Camera = pixel.IM.
		Moved(g.Window.Bounds().Center().Sub(Game.GridSizePx.Scaled(0.5))).
		Scaled(g.Window.Bounds().Center(), g.Zoom())
	g.Window.SetMatrix(g.Camera)

	Control(g.Window, dt)

	x, y := 0, 0
	x, y = IndexOnGrid(BoxOnGrid(g.Player.Box))
	g.Grid[y][x].typ = "free"
	g.Grid[y][x].occupied = false
	g.Player.Update(dt)
	x, y = IndexOnGrid(BoxOnGrid(g.Player.Box))
	g.Grid[y][x].typ = "player"
	g.Grid[y][x].occupied = true

	g.Player.Catch(Game.Folks.F)
	UpdateFolks(Game.Folks.F, dt)

	// for y := 0; y < len(g.Grid); y++ {
	// 	for x := 0; x < len(g.Grid[y]); x++ {
	// 		switch g.Grid[y][x].typ {
	// 		case "free":
	// 			g.Grid[y][x] = TileStruct{"free", false, false}
	// 		case "wall":
	// 			//g.Grid[y][x] = TileStruct{"wall", true, false}
	// 		case "player":
	// 			g.Grid[y][x] = TileStruct{"player", true, true}
	// 		case "firepit":
	// 			// firepit
	// 		case "folk":
	// 			g.Grid[y][x] = TileStruct{"folk", true, false}
	// 		default:
	// 			fmt.Printf("WARNING: Unrecognized level object - '%c' [%d, %d]\n", g.Grid[y][x], x, y)
	// 			g.Grid[y][x] = TileStruct{"free", false, false}
	// 		}
	// 	}
	// }
}

func (g *GameStruct) Draw(target pixel.Target) {
	g.FloorImd.Clear()
	siz := pixel.V(1, 1).Scaled(GridSize)
	for y := 0; y < len(g.Grid); y++ {
		for x := 0; x < len(g.Grid[y]); x++ {
			p := pixel.V(float64(x), float64(y)).Scaled(GridSize).Add(siz.Scaled(0.5))
			// g.FloorImd.Push(p)
			// g.FloorImd.Push(p.Add(pixel.V(1, 1).Scaled(GridSize)))
			DrawObjekt(p, siz, 0, g.FloorSprite, g.FloorImd)
		}
	}
	g.FloorImd.Draw(target)

	Game.Player.Draw(Game.Window)
	Game.Folks.Draw(Game.Window)
	Game.Walls.Draw(Game.Window)
	Debug(Game.Window, 1)
}

func (g *GameStruct) MoveFolks() {
	x, y := 0, 0
	for true {
		time.Sleep(time.Second / time.Duration(GameStepsPerSecond))
		for _, f := range g.Folks.F {
			if f.Caught {
				continue
			}

			x, y = IndexOnGrid(f.Box)
			g.Grid[y][x].typ = "free"
			g.Grid[y][x].occupied = false

			g.Folks.Left -= BoolToInt(f.AI(g))

			x, y = IndexOnGrid(f.Box.Moved(f.DesiredPostion.Sub(f.Box.Center())))
			g.Grid[y][x].typ = "folk"
			g.Grid[y][x].occupied = true
		}
	}
}

func (g *GameStruct) Zoom() float64 {
	return BestZoom(g.GridSizePx, g.Window.Bounds().Size())
}

func BestZoom(sizView, sizWindow pixel.Vec) float64 {
	k := sizWindow.ScaledXY(pixel.V(1/sizView.X, 1/sizView.Y))
	return math.Min(k.X, k.Y)
}
