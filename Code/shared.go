package main

import (
	"fmt"
	"image/color"
	"math"

	"github.com/faiface/pixel/pixelgl"

	"github.com/faiface/pixel"
	"github.com/faiface/pixel/text"
)

var (
	Resolution = pixel.V(1280, 720)
)

func Lerp(v0, v1, t float64) float64 {
	return (1-t)*v0 + t*v1
}

func LerpVec(v0, v1 pixel.Vec, t float64) pixel.Vec {
	d := v1.Sub(v0)
	l := d.Len()
	i := RoundTo(Lerp(0, l, t), l, 1e-3)
	return v0.Add(d.Unit().Scaled(i))
}

func RoundTo(x, round, prec float64) float64 {
	if math.Abs(x-round) < prec {
		x = round
	}
	return x
}

func BoolToInt(b bool) int {
	if b {
		return 1
	} else {
		return 0
	}
}

func FloorE(x float64, f int) float64 {
	e := math.Pow10(-f)
	return float64(int(x*e)) / e
}

func Round(x, f float64) float64 {
	d := math.Trunc(x / f)
	m := x - d*f
	l := int(math.Log10(f)) - 2
	if m < f/2 {
		return FloorE(d*f, l)
	} else {
		return FloorE((d+1)*f, l)
	}
}

func PosOnGrid(v pixel.Vec) pixel.Vec {
	return pixel.V(Round(v.X, GridSize), Round(v.Y, GridSize))
}

func BoxOnGrid(box pixel.Rect) pixel.Rect {
	return box.Moved(PosOnGrid(box.Min).Sub(box.Min))
}

func IndexOnGrid(box pixel.Rect) (int, int) {
	pos := box.Min.Scaled(1 / GridSize)
	x, y := int(Round(pos.X, 1)), int(Round(pos.Y, 1))
	return x, y
}

func BoxMovedTo(box pixel.Rect, pos pixel.Vec) pixel.Rect {
	return box.Moved(pos.Sub(box.Center()))
}

func DrawObjekt(pos, siz pixel.Vec, rot float64, sprite *pixel.Sprite, target pixel.Target) {
	mat := pixel.IM.
		Rotated(pixel.ZV, rot).
		ScaledXY(pixel.ZV, pixel.V(
			siz.X/sprite.Picture().Bounds().W(),
			siz.Y/sprite.Picture().Bounds().H(),
		)).
		Moved(pos)
	sprite.Draw(target, mat)
}

func DrawText(out string, pos, sca pixel.Vec, col color.RGBA, win *pixelgl.Window) {
	txt := text.New(pixel.V(0, 0), TextAtlas)
	txt.Color = col
	fmt.Fprintln(txt, out)
	txt.Draw(
		win,
		pixel.IM.
			ScaledXY(txt.Bounds().Center(), sca).
			Moved(pos),
	)
	win.Update()
}
